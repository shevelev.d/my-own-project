from django.contrib.auth.models import User
from django.db import models


class Comment(models.Model):
    owner = models.ForeignKey('posts.Post', on_delete=models.CASCADE)

    author = models.ForeignKey(User, on_delete=models.CASCADE)
    added = models.DateTimeField(auto_created=True, blank=True)

    content = models.TextField()

    def __str__(self):
        return '%s, %s' % (self.added, self.author)
