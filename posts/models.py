from django.contrib.auth.models import User
from django.db import models


class Post(models.Model):
    slug = models.SlugField()
    title = models.CharField(max_length=255)
    content = models.TextField()

    author = models.ForeignKey(User, on_delete=models.CASCADE)
    added = models.DateTimeField(auto_created=True, blank=True)

    def __str__(self):
        return self.title
